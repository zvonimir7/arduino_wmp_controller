﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace ArduinoLightController {
    public partial class Form : System.Windows.Forms.Form {
        public delegate void AddDataDelegate(String myString);
        public AddDataDelegate myDelegate;

        static MySerialPort _serialPort;

        private Int32 iHandle;

        public Form() {
            InitializeComponent();
            getAvailablePorts();
            initializeUI();
            //createDirectoryOnDesktop();
        }

        private void Form_Load(object sender, EventArgs e) {
            myDelegate = new AddDataDelegate(AddDataMethod);
        }

        void createDirectoryOnDesktop() {
            string path = @"C:\Users\" + Environment.UserName + "\\Desktop\\Movie Folder Goddie";
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
                MessageBox.Show("Folder for movies has been created on Your desktop");
            }

        }

        private void initializeUI() {
            btn_Play.Enabled = false;
            btn_Stop.Enabled = false;
            btn_Disconnect.Enabled = false;
            btn_Sync.Enabled = false;
            btn_Connect.Enabled = true;
        }

        void getAvailablePorts() {
            String[] ports = SerialPort.GetPortNames();
            cb_ports.Items.Clear();
            cb_ports.Items.AddRange(ports);
            cb_ports.Text = "";
        }

        private void btn_Connect_Click(object sender, EventArgs e) {
            try {
                if (cb_ports.Text == "") {
                    lbl_Errors.Text = "Please select port settings";
                } else {
                    _serialPort = new MySerialPort(cb_ports.Text);
                    _serialPort.getSerialPort().DataReceived += new SerialDataReceivedEventHandler(mySerialPort_DataReceived);
                    _serialPort.Open();
                    btn_Connect.Enabled = false;
                    btn_Disconnect.Enabled = true;
                    lbl_Errors.Text = "connection was successfully established with " + cb_ports.Text + "!";
                    btn_Sync.Enabled = true;

                    if (checkForSyncWMP()) {
                        btn_Play.Enabled = true;
                        btn_Stop.Enabled = true;
                        btn_Sync.Enabled = false;
                        bw_MovieHandleWorker.RunWorkerAsync();
                    }
                }
            }

            catch (UnauthorizedAccessException) {
                lbl_Errors.Text = "Unauthorised Access";
            }
        }

        private void btn_Disconnect_Click(object sender, EventArgs e) {
            _serialPort.Close();
            btn_Connect.Enabled = true;
            btn_Disconnect.Enabled = false;
            btn_Play.Enabled = false;
            btn_Stop.Enabled = false;
            cb_ports.Text = "";
            getAvailablePorts();
            lbl_Errors.Text = "connection was successfully terminated!";
        }

        public void AddDataMethod(String myString) {
            rtb_SerialInput.AppendText("Stanje analognog ulaza arduina je: " + myString);
            Int32.TryParse(myString, out int serialNumberRead);

            if (checkForSyncWMP()) {
                if (serialNumberRead == 3) {
                    btn_Play_Click(null, EventArgs.Empty);
                }
            }
        }

        private void mySerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e) {
            SerialPort sp = (SerialPort)sender;
            string s = sp.ReadExisting();

            rtb_SerialInput.Invoke(this.myDelegate, new Object[] { s });
        }

        private void btn_Clear_Click(object sender, EventArgs e) {
            rtb_SerialInput.Clear();
        }

        private void btn_Play_Click(object sender, EventArgs e) {
            Win32.SendMessage(iHandle, Win32.WM_COMMAND, 0x00004978, 0x00000000);
        }

        private void btn_Stop_Click(object sender, EventArgs e) {
            Win32.SendMessage(iHandle, Win32.WM_COMMAND, 0x00004979, 0x00000000);
        }

        private void PlayFile(String movieName) {
            string movieNameURL = @"C:\Users\" + Environment.UserName + "\\Desktop\\testExamples\\" + movieName + ".wmv";
        }

        private void btn_Sync_Click(object sender, EventArgs e) {
            iHandle = Win32.FindWindow("WMPlayerApp", "Windows Media Player");

            if (iHandle != 0) {
                btn_Play.Enabled = true;
                btn_Stop.Enabled = true;
                lbl_PlayerError.Text = "Connection with WMP is established!";
                bw_MovieHandleWorker.RunWorkerAsync();
            } else {
                lbl_PlayerError.Text = "You have to run movie/music/etc in Windows Media Player before syncing!";
            }
        }

        private bool checkForSyncWMP() {
            if (iHandle != 0) {
                return true;
            } else {
                return false;
            }
        }

        private void bw_MovieHandle_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e) {
            btn_Sync.Enabled = false;
            iHandle = Win32.FindWindow("WMPlayerApp", "Windows Media Player");
        }

        private void bw_MovieHandle_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e) {
            if (_serialPort.getSerialPort().IsOpen) {
                if (!checkForSyncWMP()) {
                    btn_Play.Enabled = false;
                    btn_Stop.Enabled = false;
                    lbl_PlayerError.Text = "Windows Media Player is closed!";
                    btn_Sync.Enabled = true;
                } else {
                    bw_MovieHandleWorker.RunWorkerAsync();
                }
            } else {
                lbl_Errors.Text = "USB Cable is disconnected!";
                getAvailablePorts();
                initializeUI();
                btn_Sync.Enabled = false;
            }
        }

        private void btn_Refresh_Click(object sender, EventArgs e) {
            getAvailablePorts();
        }
    }
}