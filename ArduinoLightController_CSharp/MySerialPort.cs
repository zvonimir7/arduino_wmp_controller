﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading.Tasks;

namespace ArduinoLightController {
    class MySerialPort {
        private static SerialPort _serialPort;

        public MySerialPort(String portName) {
            _serialPort = new SerialPort();
            _serialPort.PortName = portName;
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
        }

        public void Open() {
            _serialPort.Open();
        }

        public void Close() {
            _serialPort.Close();
        }

        public SerialPort getSerialPort() {
            return _serialPort;
        }
    }
}
