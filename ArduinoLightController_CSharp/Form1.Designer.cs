﻿
namespace ArduinoLightController {
    partial class Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.cb_ports = new System.Windows.Forms.ComboBox();
            this.btn_Disconnect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Errors = new System.Windows.Forms.Label();
            this.rtb_SerialInput = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Play = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.lbl_PlayerError = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Sync = new System.Windows.Forms.Button();
            this.bw_MovieHandleWorker = new System.ComponentModel.BackgroundWorker();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.btn_Refresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cb_ports
            // 
            this.cb_ports.FormattingEnabled = true;
            this.cb_ports.Location = new System.Drawing.Point(12, 12);
            this.cb_ports.Name = "cb_ports";
            this.cb_ports.Size = new System.Drawing.Size(121, 24);
            this.cb_ports.TabIndex = 0;
            // 
            // btn_Disconnect
            // 
            this.btn_Disconnect.Location = new System.Drawing.Point(156, 43);
            this.btn_Disconnect.Name = "btn_Disconnect";
            this.btn_Disconnect.Size = new System.Drawing.Size(101, 27);
            this.btn_Disconnect.TabIndex = 2;
            this.btn_Disconnect.Text = "Disconnect";
            this.btn_Disconnect.UseVisualStyleBackColor = true;
            this.btn_Disconnect.Click += new System.EventHandler(this.btn_Disconnect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Progress:";
            // 
            // lbl_Errors
            // 
            this.lbl_Errors.AutoSize = true;
            this.lbl_Errors.Location = new System.Drawing.Point(382, 15);
            this.lbl_Errors.Name = "lbl_Errors";
            this.lbl_Errors.Size = new System.Drawing.Size(42, 17);
            this.lbl_Errors.TabIndex = 4;
            this.lbl_Errors.Text = "None";
            // 
            // rtb_SerialInput
            // 
            this.rtb_SerialInput.Location = new System.Drawing.Point(339, 155);
            this.rtb_SerialInput.Name = "rtb_SerialInput";
            this.rtb_SerialInput.Size = new System.Drawing.Size(478, 311);
            this.rtb_SerialInput.TabIndex = 5;
            this.rtb_SerialInput.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(336, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Serial Port Input:";
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(716, 122);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(101, 27);
            this.btn_Clear.TabIndex = 7;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Play
            // 
            this.btn_Play.Location = new System.Drawing.Point(12, 406);
            this.btn_Play.Name = "btn_Play";
            this.btn_Play.Size = new System.Drawing.Size(101, 27);
            this.btn_Play.TabIndex = 8;
            this.btn_Play.Text = "Play";
            this.btn_Play.UseVisualStyleBackColor = true;
            this.btn_Play.Click += new System.EventHandler(this.btn_Play_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(12, 439);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(101, 27);
            this.btn_Stop.TabIndex = 9;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // lbl_PlayerError
            // 
            this.lbl_PlayerError.AutoSize = true;
            this.lbl_PlayerError.Location = new System.Drawing.Point(84, 334);
            this.lbl_PlayerError.Name = "lbl_PlayerError";
            this.lbl_PlayerError.Size = new System.Drawing.Size(42, 17);
            this.lbl_PlayerError.TabIndex = 11;
            this.lbl_PlayerError.Text = "None";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 334);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Progress:";
            // 
            // btn_Sync
            // 
            this.btn_Sync.Location = new System.Drawing.Point(12, 373);
            this.btn_Sync.Name = "btn_Sync";
            this.btn_Sync.Size = new System.Drawing.Size(101, 27);
            this.btn_Sync.TabIndex = 12;
            this.btn_Sync.Text = "Sync";
            this.btn_Sync.UseVisualStyleBackColor = true;
            this.btn_Sync.Click += new System.EventHandler(this.btn_Sync_Click);
            // 
            // bw_MovieHandleWorker
            // 
            this.bw_MovieHandleWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_MovieHandle_DoWork);
            this.bw_MovieHandleWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_MovieHandle_RunWorkerCompleted);
            // 
            // btn_Connect
            // 
            this.btn_Connect.Location = new System.Drawing.Point(156, 10);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(101, 27);
            this.btn_Connect.TabIndex = 13;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Location = new System.Drawing.Point(12, 43);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(121, 27);
            this.btn_Refresh.TabIndex = 14;
            this.btn_Refresh.Text = "Refresh";
            this.btn_Refresh.UseVisualStyleBackColor = true;
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 478);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_Connect);
            this.Controls.Add(this.btn_Sync);
            this.Controls.Add(this.lbl_PlayerError);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.btn_Play);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtb_SerialInput);
            this.Controls.Add(this.lbl_Errors);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Disconnect);
            this.Controls.Add(this.cb_ports);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form";
            this.ShowIcon = false;
            this.Text = "Arduino Light Controller";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_ports;
        private System.Windows.Forms.Button btn_Disconnect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Errors;
        private System.Windows.Forms.RichTextBox rtb_SerialInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Play;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Label lbl_PlayerError;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Sync;
        private System.ComponentModel.BackgroundWorker bw_MovieHandleWorker;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.Button btn_Refresh;
    }
}

