const int photoResistorPin = A0;
int photoResistorValue;

const int firstLedPin=7;
const int secondLedPin=5;
const int thirdPin=3;
int ledCurrentState = 0;

void setup() {
  pinMode(firstLedPin, OUTPUT);
  pinMode(secondLedPin, OUTPUT);
  pinMode(thirdPin, OUTPUT);
  pinMode(photoResistorPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  photoResistorValue = analogRead(photoResistorPin);

  if ((photoResistorValue < 350) && (photoResistorValue > 200)) {
    if (ledCurrentState != 1) {
      changeLEDState(1);
    }
  } else if ((photoResistorValue <= 200) && (photoResistorValue > 100)) {
    if (ledCurrentState != 2) {
      changeLEDState(2);
    }
  } else if (photoResistorValue <= 100) {
    if (ledCurrentState != 3) {
      changeLEDState(3);
    }
  } else {
    if (ledCurrentState != 0) {
      changeLEDState(0);
    }
  }

  delay(1000);
}

void changeLEDState(int state) {
  if (state == 1) {
    digitalWrite(firstLedPin, HIGH);
    digitalWrite(secondLedPin, LOW);
    digitalWrite(thirdPin, LOW);
    ledCurrentState = 1;
  } else if (state == 0) {
    digitalWrite(firstLedPin, LOW);
    digitalWrite(secondLedPin, LOW);
    digitalWrite(thirdPin, LOW);
    ledCurrentState = 0;
  } else if (state == 2) {
    digitalWrite(firstLedPin, HIGH);
    digitalWrite(secondLedPin, HIGH);
    digitalWrite(thirdPin, LOW);
    ledCurrentState = 2;
  } else if (state == 3) {
    digitalWrite(firstLedPin, HIGH);
    digitalWrite(secondLedPin, HIGH);
    digitalWrite(thirdPin, HIGH);
    ledCurrentState = 3;
  }

  Serial.println(ledCurrentState);
}
